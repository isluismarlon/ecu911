odoo.define('helpdesk.coordenate', function (require) {
    "use strict";
    var utils = require('web.utils');
    var Model = require('web.Model');
    var core = require('web.core');
    var form_widget = require('web.form_widgets');
    var Corrective = new Model('helpdesk.corrective')
    var Relocation = new Model('helpdesk.relocation')
    var Preventive = new Model('helpdesk.preventive')

    // do things with utils and Model

    function errores(error) {

        var mensaje;
        switch(error.code) {
            case error.PERMISSION_DENIED:
                mensaje = "User denied the request for Geolocation."
                break;
            case error.POSITION_UNAVAILABLE:
                mensaje = "Location information is unavailable."
                break;
            case error.TIMEOUT:
                mensaje = "The request to get user location timed out."
                break;
            case error.UNKNOWN_ERROR:
                mensaje = "An unknown error occurred."
                break;
        }
        alert(mensaje);
    }

    form_widget.WidgetButton.include({
        on_click: function() {
            if(this.node.attrs.custom === "coordenadas"){
                if (navigator.geolocation) {
                    var coord = navigator.geolocation.getCurrentPosition(this.proxy('showPosition'),errores);
                } else {
                    alert("Geolocation is not supported by this browser.");
                    return;
                }
            }
            this._super();
        },
    });
    var WidgetButton = form_widget.WidgetButton.extend({
        showPosition: function(position){
            // alert("estmos adentro");
            // Corrective.call('set_coordenate',[id,position.coords.latitude,position.coords.longitude])
            if (this.view.model === 'helpdesk.corrective') {
                Corrective.call('set_coordenate', [this.view.datarecord.id, position.coords.latitude, position.coords.longitude])
            }
            if (this.view.model === 'helpdesk.preventive') {
                Preventive.call('set_coordenate', [this.view.datarecord.id, position.coords.latitude, position.coords.longitude])
            }
            if (this.view.model === 'helpdesk.relocation') {
                Relocation.call('set_coordenate', [this.view.datarecord.id, position.coords.latitude, position.coords.longitude])
            }
            alert('Coordenadas actualizadas')
            this.view.reload();
            // this._super();
        },
    });
    core.form_tag_registry.add('button', WidgetButton);

    return {
        WidgetButton: WidgetButton
    };
});