# -*- coding: utf-8 -*-

from odoo import fields, models, tools, api


class TicketReportView(models.Model):
    _name = 'helpdesk.ticket.report.view'
    _auto = False
    _rec_name = 'report_id'

    date = fields.Date(string=u'Fecha')
    center_id = fields.Many2one('helpdesk.center', string=u'Centro')
    ticket_id = fields.Many2one('helpdesk.ticket', string=u'Ticket Nro.')
    report_id = fields.Char(string=u'Informe Nro.')
    report_type = fields.Char(string=u'Tipo de informe')
    stage_id = fields.Many2one('helpdesk.stage', string='Etapa')
    point_id = fields.Many2one('helpdesk.point', string='Punto')



    # def _select(self):
    #     select_str = """
    #     min(ps.id) as id,psl.name as tipo,psl.code as code,emp.id as name,jb.id as job_id,
    #     dp.id as department_id,cmp.id as company_id,
    #     ps.date_from, ps.date_to, sum(psl.total) as net, ps.state as state
    #     """
    #     return select_str
    #
    # def _from(self):
    #     from_str = """
    #         hr_payslip_line psl  join hr_payslip ps on (ps.employee_id=psl.employee_id and ps.id=psl.slip_id)
    #         join hr_employee emp on (ps.employee_id=emp.id) join hr_department dp on (emp.department_id=dp.id)
    #         join hr_job jb on (emp.department_id=jb.id) join res_company cmp on (cmp.id=ps.company_id)
    #      """
    #     return from_str
    #
    # def _group_by(self):
    #     group_by_str = """
    #         group by emp.id,psl.total,ps.date_from, ps.date_to, ps.state,jb.id,dp.id,cmp.id,psl.name,psl.code
    #     """
    #     return group_by_str
    #
    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""CREATE or REPLACE VIEW %s as (
               SELECT
                  helpdesk_corrective.ticket_id as id, 
                  helpdesk_corrective.date as date,
                  helpdesk_corrective.center_id as center_id,
                  helpdesk_corrective.ticket_id as ticket_id,
                  helpdesk_corrective.name as report_id,
                  'Correctivo' as report_type,
                  helpdesk_corrective.point_id as point_id,
                  helpdesk_corrective.stage_id as stage_id
                FROM
                  public.helpdesk_corrective
                UNION
                SELECT
                  helpdesk_preventive.id * - 1 as id, 
                  helpdesk_preventive.date as date,
                  helpdesk_preventive.center_id as center_id,
                  helpdesk_preventive.ticket_id as ticket_id,
                  helpdesk_preventive.name as report_id,
                  'Preventivo' as report_type,
                  helpdesk_preventive.point_id as point_id,
                  helpdesk_preventive.stage_id as stage_id
                FROM
                  public.helpdesk_preventive
                UNION
                SELECT
                  helpdesk_camera_maintenance.ticket_id as id, 
                  helpdesk_camera_maintenance.start_date as date,
                  helpdesk_camera_maintenance.center_id as center_id,
                  helpdesk_camera_maintenance.ticket_id as ticket_id,
                  helpdesk_camera_maintenance.name as report_id,
                  'Reparación de Camaras' as report_type,
                  Null as point_id,
                  helpdesk_camera_maintenance.stage_id as stage_id
                FROM 
                  public.helpdesk_camera_maintenance
                UNION
                SELECT
                  helpdesk_corrective_cabinet.ticket_id as id, 
                  helpdesk_corrective_cabinet.date as date,
                  helpdesk_corrective_cabinet.center_id as center_id,
                  helpdesk_corrective_cabinet.ticket_id as ticket_id,
                  helpdesk_corrective_cabinet.name as report_id,
                  'Correctivo Gabinete' as report_type,
                  helpdesk_corrective_cabinet.point_id as point_id,
                  helpdesk_corrective_cabinet.stage_id as stage_id
                FROM 
                  public.helpdesk_corrective_cabinet
                UNION
                SELECT
                  helpdesk_relocation.ticket_id as id, 
                  helpdesk_relocation.date as date,
                  helpdesk_relocation.center_id as center_id,
                  helpdesk_relocation.ticket_id as ticket_id,
                  helpdesk_relocation.name as report_id,
                  'Reubicacion Punto Video Vigilancia' as report_type,
                  helpdesk_relocation.point_id as point_id,
                  helpdesk_relocation.stage_id as stage_id
                FROM 
                  public.helpdesk_relocation
                UNION
                SELECT
                  helpdesk_poste_maintenance.ticket_id as id, 
                  helpdesk_poste_maintenance.taking_date as date,
                  helpdesk_poste_maintenance.center_id as center_id,
                  helpdesk_poste_maintenance.ticket_id as ticket_id,
                  helpdesk_poste_maintenance.name as report_id,
                  'Postes' as report_type,
                  helpdesk_poste_maintenance.point_id as point_id,
                  helpdesk_poste_maintenance.stage_id as stage_id
                FROM 
                  public.helpdesk_poste_maintenance
               )""" % (self._table))
