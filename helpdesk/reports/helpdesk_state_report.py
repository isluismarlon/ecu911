# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, tools

ATTETION_TIME = [('49', 'More 48'),
                ('48', 'Between 24 and 48'),
                ('24', 'Less 24')]

class HelpdeskStateReport(models.Model):
    """ Clientes por Provincia"""

    _name = "helpdesk.state.report"
    _auto = False
    _description = "Clientes por Provincia"
    _rec_name = 'helpdesk_ticket_id'

    date_closed = fields.Datetime('Fecha de cierre', readonly=True)
    created_date = fields.Datetime('Fecha de apertura', readonly=True)
    partner_id = fields.Many2one('res.partner', 'Cliente', readonly=True)
    helpdesk_ticket_id = fields.Many2one('helpdesk.ticket', string='Ticket')
    city_id = fields.Many2one('res.state.city', 'Ciudad', readonly=True)
    state_id = fields.Many2one("res.country.state", string='Provincia',readonly=True)
    attention_time = fields.Selection(ATTETION_TIME, string=u"Tiempo de atención",
                                      readonly=True)
    ticket_type_id = fields.Many2one('helpdesk.ticket.type', string='Tipo de ticket', readonly=True)

    def init(self):
        tools.drop_view_if_exists(self._cr, 'helpdesk_state_report')
        self._cr.execute("""
            CREATE VIEW helpdesk_state_report AS (
                select
                    m.id,
                    m.date_closed,
                    m.created_date,
                    m.partner_id,
                    m.id as helpdesk_ticket_id,
                    m.attention_time,
                    m.ticket_type_id,
                    l.city_id,
                    l.state_id
                from
                    "helpdesk_ticket" m
                join
                    "res_partner" l
                on
                    (m.partner_id = l.id))
                """)