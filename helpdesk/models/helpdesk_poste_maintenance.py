# -*- coding: utf-8 -*-

from odoo import fields, models, api, SUPERUSER_ID
from odoo.exceptions import UserError
from odoo.tools.translate import _
from odoo.exceptions import ValidationError


YES_NO = [
    ('yes', 'Si'),
    ('no', 'No')
]


class HelpdeskPosteMaintenance(models.Model):
    _name = 'helpdesk.poste.maintenance'
    _inherits = {'helpdesk.ticket': 'ticket_id',
                 'helpdesk.point': 'point_id'}

    def _default_stage_id(self):
        search_domain = [('sequence', '>=', 0)]
        return self.env['helpdesk.stage'].search(search_domain, order='sequence', limit=1)

    name = fields.Char(string=u'ID', readonly=True, track_visibility='onchange')
    active = fields.Boolean(default=True, track_visibility='onchange')
    ticket_id = fields.Many2one('helpdesk.ticket',string=u'No. de ticket', required=True, ondelete='cascade', track_visibility='onchange')
    preventive_id = fields.Many2one('helpdesk.preventive',string=u'Informe técnico de Origen')
    # INFORMACIÓN DEL CENTRO
    center_id = fields.Many2one('helpdesk.center', string='Centro', store=True, required=True, ondelete='restrict', track_visibility='onchange')
    point_id = fields.Many2one('helpdesk.point',string=u'Punto', track_visibility='onchange', store=True, required=True, ondelete='restrict')
    delegate_id = fields.Many2one('res.users', string=u'Delegado', related='point_id.center_id.delegate_id',
                                  readonly=True, store=True)
    technical_id = fields.Many2one('helpdesk.technical', string=u'Técnico', track_visibility='onchange')
    address = fields.Char(related='point_id.address', string=u'Dirección', readonly=True, track_visibility='onchange')
    manager_approval = fields.Selection(YES_NO, string=u'Aprobación de reemplazo administrador', track_visibility='onchange')
    technical_specification = fields.Text(u'Especificaciones técnicas del poste y brazo', track_visibility='onchange')
    # INFORMACIÓN DEL MANTENIMIENTO CORRECTIVO
    installation_date = fields.Date(u'Fecha de instalación del poste', track_visibility='onchange')
    taking_date = fields.Date(u'Fecha de retiro del poste', track_visibility='onchange')
    has_technical_spe = fields.Selection(YES_NO, string=u'Cumple especificaciones técnicas poste', track_visibility='onchange')
    delegate_approval = fields.Selection(YES_NO, string=u'Aprobación del delegado', track_visibility='onchange')

    # brazo
    installation_date_arm = fields.Date(u'Fecha de instalación del brazo', track_visibility='onchange')
    taking_date_arm = fields.Date(u'Fecha de retiro del brazo', track_visibility='onchange')
    has_technical_spe_arm = fields.Selection(YES_NO, string=u'Cumple especificaciones técnicas brazo', track_visibility='onchange')


    # fotos

    box_type = fields.Selection(related='point_id.box_type', string=u'Tipo de Caja')
    poste_photo_before = fields.Binary(u'Foto Poste antes')
    poste_photo_before_name = fields.Char()
    poste_photo_after = fields.Binary(u'Foto Poste después')
    poste_photo_after_name = fields.Char()
    box_electric_photo_before = fields.Binary(u'Foto eléctrica antes')
    box_electric_photo_before_name = fields.Char()
    box_electric_photo_after = fields.Binary(u'Foto eléctrica después')
    box_electric_photo_after_name = fields.Char()
    box_data_photo_before = fields.Binary(u'Foto datos antes')
    box_data_photo_before_name = fields.Char()
    box_data_photo_after = fields.Binary(u'Foto datos después')
    box_data_photo_after_name = fields.Char()
    box_photo_before = fields.Binary(u'Foto única antes')
    box_photo_before_name = fields.Char()
    box_photo_after = fields.Binary(u'Foto única después')
    box_photo_after_name = fields.Char()
    ground_photo_before = fields.Binary(u'Foto Tierra antes')
    ground_photo_before_name = fields.Char()
    ground_photo_after = fields.Binary(u'Foto Tierra después')
    ground_photo_after_name = fields.Char()

    # fotos brazo
    arm_photo_before = fields.Binary(u'Foto Brazo antes')
    arm_photo_before_name = fields.Char()
    arm_photo_after = fields.Binary(u'Foto Brazo después')
    arm_photo_after_name = fields.Char()


    @api.model
    def create(self, vals):
        vals['name'] = self.env['ir.sequence'].next_by_code('helpdesk.poste.maintenance')
        if vals.get('point_id'):
            point = self.env['helpdesk.point'].browse(vals.get('point_id'))
            vals['delegate_id'] = point.center_id.delegate_id.id
        return super(HelpdeskPosteMaintenance, self).create(vals)

    @api.multi
    @api.constrains('stage_id')
    def check_stage(self):
        for record in self:
            if record.stage_id:
                if self.env.ref('helpdesk.group_helpdesk_technical') in self.env.user.groups_id and record.stage_id.sequence > 2:
                    raise ValidationError('No estas autorizado para a cambiar a %s' % record.stage_id.name)

    @api.model
    def _read_group_stage_ids(self, stages, domain, order):
        search_domain = [('sequence', '>=', 0)]
        stage_ids = stages._search(search_domain, order=order, access_rights_uid=SUPERUSER_ID)
        return stages.browse(stage_ids)