# -*- coding: utf-8 -*-

from odoo import api, models, fields


class ContactWizard(models.TransientModel):
    _name = "contact.wizard"
    _description = "List of contacts"

    name = fields.Char(string=u'Nombre')


