# -*- coding: utf-8 -*-

from odoo import fields, models, api

CAMERA_TYPE = [
    ('domo_hd', u'DOMO HD'),
    ('domo_sd', u'DOMO SD'),
    ('fixed', u'FIJA'),
    ('long', u'LARGO'),
    ('aperture', u'ALCANCE'),
    ('laser', u'LASER'),
    ('plate', u'PLACAS'),
    ('thermal', u'TERMICA'),
    ('mini', u'MINIDOMO'),
    ('speeddome', u'SPEEDDOME')
]

BRAND = [
    ('huawei', u'Huawei'),
    ('tiandy', u'Tiandy'),
    ('ceiec', u'Ceiec'),
    ('flir', u'Flir'),
    ('znv', u'ZNV'),
    ('visualint', u'Visualint'),
    ('vidicam',u'Vidicam'),
]


class HelpdeskRelocationCamera(models.Model):
    _name = 'helpdesk.relocation.camera'

    relocation_id = fields.Many2one('helpdesk.relocation')
    camera_id = fields.Many2one('helpdesk.camera', string=u'Cámara')
    camera_type = fields.Selection(CAMERA_TYPE, string=u'Tipo')
    ip_address = fields.Char(string=u'IP')
    brand = fields.Selection(BRAND, string=u'Marca')
    model = fields.Char(string=u'Modelo')
    photo_before = fields.Binary(u'Foto antes')
    photo_before_name = fields.Char()
    photo_after = fields.Binary(u'Foto después')
    photo_after_name = fields.Char()
    n_serial_1 = fields.Char(string=u'Nro. Serial 1')
    n_serial_2 = fields.Char(string=u'Nro. Serial 2')
    l_d_i_e = fields.Boolean(string=u'Limpieza de domo interno y externo')
    l_l_c = fields.Boolean(string=u'Limpieza de lente de la cámara')
    d_u_a = fields.Boolean(string=u'Domo ubicado y asegurado')
    t_c_l = fields.Boolean(string=u'Tapa de cámara limpia')
    t_c_a = fields.Boolean(string=u'Tapa de cámara asegurada')
    c_a = fields.Boolean(string=u'Cámara asegurada')
    c_b_h = fields.Boolean(string=u'Cables bajo herraje')
    c_e = fields.Boolean(string=u'Cámara energizada')
    p_v_v = fields.Boolean(string=u'Pruebas con video vigilancia')
    r_f = fields.Boolean(string=u'Revisado filtraciones')
    r_o = fields.Boolean(string=u'Revisado oxidación')
    r_u = fields.Boolean(string=u'Revisión de ubicación')

    @api.model
    def create(self, vals):
        if vals.get('camera_id'):
            camera = self.env['helpdesk.camera'].browse(vals.get('camera_id'))
            vals['ip_address'] = camera.ip_number
            vals['camera_type'] = camera.camera_type
            vals['n_serial_1'] = camera.serial_1
            vals['n_serial_2'] = camera.serial_2
            vals['brand'] = camera.brand
            vals['model'] = camera.model
        return super(HelpdeskRelocationCamera, self).create(vals)

    @api.multi
    def write(self, vals):
        camera_id = vals.get('camera_id', self.camera_id.id)
        if camera_id:
            camera = self.env['helpdesk.camera'].browse(camera_id)
            camera_values = {}
            if vals.get('ip_address'):
                camera_values['ip_number'] = vals['ip_address']
            if vals.get('type'):
                camera_values['camera_type'] = vals['type']
            if vals.get('n_serial_1'):
                camera_values['serial_1'] = vals['n_serial_1']
            if vals.get('n_serial_2'):
                camera_values['serial_2'] = vals['n_serial_2']
            if vals.get('brand'):
                camera_values['brand'] = vals['brand']
            if vals.get('model'):
                camera_values['model'] = vals['model']
            camera.sudo().write(camera_values)
        return super(HelpdeskRelocationCamera, self).write(vals)



