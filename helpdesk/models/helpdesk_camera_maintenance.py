# -*- coding: utf-8 -*-

from odoo import fields, models, api, SUPERUSER_ID
from odoo.exceptions import UserError
from odoo.tools.translate import _
from odoo.exceptions import ValidationError

YES_NO = [
    ('yes', 'Si'),
    ('no', 'No')
]

CAMERA_TYPE = [
    ('domo_hd', u'DOMO HD'),
    ('domo_sd', u'DOMO SD'),
    ('fixed', u'FIJA'),
    ('long', u'LARGO'),
    ('aperture', u'ALCANCE'),
    ('laser', u'LASER'),
    ('plate', u'PLACAS'),
    ('thermal', u'TERMICA'),
    ('mini', u'MINIDOMO'),
    ('speeddome', u'SPEEDDOME')
]
BRAND = [
    ('huawei', u'Huawei'),
    ('tiandy', u'Tiandy'),
    ('ceiec', u'Ceiec'),
    ('flir', u'Flir'),
    ('znv', u'ZNV'),
    ('visualint', u'Visualint'),
]



class HelpdeskCameraMaintenance(models.Model):
    _name = 'helpdesk.camera.maintenance'
    _inherits = {'helpdesk.ticket': 'ticket_id'}


    name = fields.Char(string=u'ID', readonly=True, track_visibility='onchange')
    active = fields.Boolean(default=True, track_visibility='onchange')
    ticket_id = fields.Many2one('helpdesk.ticket',string=u'No. de ticket', required=True, ondelete='cascade', track_visibility='onchange')
    # INFORMACIÓN DEL CENTRO
    center_id = fields.Many2one('helpdesk.center', string='Centro', store=True, required=True, ondelete='restrict', track_visibility='onchange')
    technical_id = fields.Many2one('helpdesk.technical', string=u'Técnico', track_visibility='onchange')
    start_date = fields.Date(string=u'Fecha de inicio de reparación', track_visibility='onchange')
    special_camera = fields.Selection(YES_NO, string=u'Cámara especial', track_visibility='onchange')
    quotation_number = fields.Char(u'Número de cotización', track_visibility='onchange')
    quotation_value = fields.Char(u'Valor de cotización', track_visibility='onchange')
    manager_approval = fields.Selection(YES_NO, string=u'Aprobación de administrador', track_visibility='onchange')

    # INFORMACIÓN DE LA CÁMARA
    serial_1 = fields.Char(string=u'Serial 1', track_visibility='onchange')
    serial_2 = fields.Char(string=u'Serial 2', track_visibility='onchange')
    serial_campana =  fields.Char(string=u'Serial Campana', track_visibility='onchange')
    brand = fields.Selection(BRAND, string="Marca", track_visibility='onchange')
    model = fields.Char(string="Modelo", track_visibility='onchange')
    camera_type = fields.Selection(CAMERA_TYPE, string=u'Tipo', track_visibility='onchange')
    camera_components = fields.Char(string=u'Componentes de la cámara', track_visibility='onchange')
    camera_takendown = fields.Selection(YES_NO, string=u'Cámara debe ser dada de baja', track_visibility='onchange')
    camera_takendown_report = fields.Text(string=u'Reporte de baja de cámara', track_visibility='onchange')
    repairing_description = fields.Text(string=u'Descripción de reparación', track_visibility='onchange')
    used_item = fields.Text(string=u'Items usados', track_visibility='onchange')
    delivery_date = fields.Date(string=u'Fecha de entrega de cámara', track_visibility='onchange')
    delegate_approval = fields.Selection(YES_NO, string=u'Aprobación de funcionamiento delegado', track_visibility='onchange')
    address = fields.Char(u'Dirección')


    # imagenes
    image_ids = fields.One2many('helpdesk.image', inverse_name='camera_maintenance_id', track_visibility='onchange')
    @api.model
    def create(self, vals):
        vals['name'] = self.env['ir.sequence'].next_by_code('helpdesk.camera.maintenance')
        if vals.get('point_id'):
            point = self.env['helpdesk.point'].browse(vals.get('point_id'))
            vals['delegate_id'] = point.center_id.delegate_id.id
            vals['address'] = point.center_id.address

        return super(HelpdeskCameraMaintenance, self).create(vals)

    @api.multi
    @api.constrains('stage_id')
    def check_stage(self):
        for record in self:
            if record.stage_id:
                if self.env.ref('helpdesk.group_helpdesk_technical') in self.env.user.groups_id and record.stage_id.sequence > 2:
                    raise ValidationError('No estas autorizado para a cambiar a %s' % record.stage_id.name)
