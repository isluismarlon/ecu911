# -*- coding: utf-8 -*-

from odoo import fields, models, api, SUPERUSER_ID
import base64
import cStringIO
from . import ImageMetaData

STATE_CAMERA = [
    ('dome', u'Domo'),
    ('fixed', u'Fija')
]

POSTE_TYPE = [
    ('concrete', u'Hormigón'),
    ('metal', u'Metálico'),
    ('fiber', u'Fibra de vidrio'),
    ('no_exist', u'No existe'),
]

BOX_TYPE = [
    ('electric_data', u'Eléctrica y datos'),
    ('unique', u'Única')
]

ARM_POSITION = [
    ('poste', u'Poste'),
    ('wall', u'Pared')
]

WORK_DONE = [
    ('1', u'Mantenimiento'),
    ('2', u'Reparación'),
    ('3', u'Reemplazo')
]

YES_NO = [
    ('yes', 'Si'),
    ('no', 'No')
]


class HelpdeskRelocation(models.Model):
    _name = 'helpdesk.relocation'
    _inherit = ['mail.thread']

    def _default_stage_id(self):
        search_domain = [('sequence', '>=', 0)]
        return self.env['helpdesk.stage'].search(search_domain, order='sequence', limit=1)

    # INFORMACIÓN DE PUNTO DE REVISIÓN
    name = fields.Char(string=u'ID', readonly=True)
    active = fields.Boolean(default=True, track_visibility='onchange')
    stage_id = fields.Many2one('helpdesk.stage', string='Stage', track_visibility='onchange', index=True ,default=_default_stage_id, group_expand='_read_group_stage_ids')
    delegate_id = fields.Many2one('res.users', string=u'Delegado',
                                  readonly=True, store=True)
    delegate_phone = fields.Char(string=u'Teléfono del delegado')
    technical_id = fields.Many2one('helpdesk.technical', string=u'Técnico', track_visibility='onchange')
    ticket_id = fields.Many2one('helpdesk.ticket', string="Ticket", ondelete='restrict', required=True,
                                domain="[('ticket_type_id.code','=','mp')]" )
    ticket_name = fields.Char(related='ticket_id.name')
    point_id = fields.Many2one('helpdesk.point', string=u'Punto')
    center_id = fields.Many2one('helpdesk.center',related='point_id.center_id', readonly=True, string=u'Centro', store=True)
    address = fields.Char(u'Dirección')
    address_reference = fields.Text(u'Referencia Ubicación')
    date = fields.Date(u'Fecha')
    state_id = fields.Many2one('res.country.state', string=u'Provincia')
    city_id = fields.Many2one('res.state.city', string=u'Cantón')
    latitude = fields.Char(u'Latitud')
    longitude = fields.Char(u'Longitud')

    latitude_after = fields.Char(u'Latitud despues')
    longitude_after = fields.Char(u'Longitud despues')

    ecu_user = fields.Char(u'Técnico de ECU911 que realiza la prueba')
    pvo = fields.Selection(YES_NO, string=u'Punto de vigilancia operativo', default='no')
    image_pin_response_photo = fields.Binary(string=u'Imagen de la cámara y pin de respuesta')
    image_pin_response_photo_name = fields.Char()

    # INFORMACIÓN GENERAL DE COMPONENTES
    camera_change = fields.Boolean(u'Requiere cambio de cámara')
    poste_change = fields.Boolean(u'Requiere cambio de poste')
    arm_change = fields.Boolean(u'Requiere cambio de brazo')
    box_change = fields.Boolean(u'Requiere cambio de caja')
    diagnostic_camera = fields.Binary(string=u'Diagnóstico', help='Si es positivo adjuntar foto o archivo')
    diagnostic_camera_name = fields.Char()
    diagnostic_poste = fields.Binary(string=u'Diagnóstico', help='Si es positivo adjuntar foto o archivo')
    diagnostic_poste_name = fields.Char()
    diagnostic_arm = fields.Binary(string=u'Diagnóstico', help='Si es positivo adjuntar foto o archivo')
    diagnostic_arm_name = fields.Char()
    diagnostic_box = fields.Binary(string=u'Diagnóstico', help='Si es positivo adjuntar foto o archivo')
    diagnostic_box_name = fields.Char()

    #REGISTRO DE REPUESTOS CAMBIADOS
    registry_ids = fields.One2many('helpdesk.relocation.repair.registry', inverse_name='relocation_id', copy=True)

    # MANTENIMIENTO PREVENTIVO DE CÁMARAS
    relocation_camera_ids = fields.One2many('helpdesk.relocation.camera', inverse_name='relocation_id', copy=True)

    # MANTENIMIENTO PREVENTIVO DE CAJAS
    box_type = fields.Selection(related='point_id.box_type', string=u'Tipo')
    box_electric_photo_before = fields.Binary(u'Foto eléctrica antes')
    box_electric_photo_before_name = fields.Char()
    box_electric_photo_after = fields.Binary(u'Foto eléctrica después')
    box_electric_photo_after_name = fields.Char()
    box_data_photo_before = fields.Binary(u'Foto datos antes')
    box_data_photo_before_name = fields.Char()
    box_data_photo_after = fields.Binary(u'Foto datos después')
    box_data_photo_after_name = fields.Char()
    box_photo_before=fields.Binary(u'Foto única antes')
    box_photo_before_name = fields.Char()
    box_photo_after = fields.Binary(u'Foto única después')
    box_photo_after_name = fields.Char()
    c_e = fields.Boolean(string=u'Caja etiquetada')
    u_p_s_e = fields.Boolean(string=u'UPS etiquetado')
    b_e_b = fields.Boolean(string=u'Breakers etiquetados')
    c_e_e = fields.Boolean(string=u'Cable eléctrico etiquetado')
    t_c_e = fields.Boolean(string=u'Toma corriente etiquetado')
    o_odf = fields.Boolean(string=u'ODF: ODF-1')
    o_ont = fields.Boolean(string=u'ONT: ONT-1')
    o_fot = fields.Boolean(string=u'FOT: FOT-1')
    other = fields.Boolean(string=u'Otros')
    v_a = fields.Boolean(string=u'Voltaje de acometida')
    v_a_photo = fields.Binary(u'Foto voltaje acometida')
    v_a_photo_name = fields.Char(u'Foto voltaje acometida')
    v_t_5 = fields.Boolean(string=u'Voltaje de transformador 5V')
    v_t_5_photo = fields.Binary(u'Voltaje de transformador 5V')
    v_t_5_photo_name = fields.Char(u'Foto transformador 5V')
    v_t_12 = fields.Boolean(string=u'Voltaje de transformador 12V')
    v_t_12_photo = fields.Binary(u'Voltaje de transformador 12V')
    v_t_12_photo_name = fields.Char(u'Foto transformador 12V')
    v_t_24 = fields.Boolean(string=u'Voltaje de transformador 24V AC')
    v_t_24_photo = fields.Binary(u'Voltaje de transformador 24V')
    v_t_24_photo_name = fields.Char(u'Foto transformador 24V')
    r_f = fields.Boolean(string=u'Revisado filtraciones')
    r_s = fields.Boolean(string=u'Requiere sellado')
    r_o = fields.Boolean(string=u'Revisado Oxidación')
    r_u = fields.Boolean(string=u'Revisión de ubicación')
    s_a = fields.Boolean(string=u'Sujeción adecuada')

    # MANTENIMIENTO PREVENTIVO DE POSTE/BRAZOS
    poste_type = fields.Selection(POSTE_TYPE, string=u'Tipo')
    poste_photo_before = fields.Binary(u'Foto antes')
    poste_photo_before_name = fields.Char()
    poste_photo_after = fields.Binary(u'Foto después')
    poste_photo_after_name = fields.Char()
    r_oxd = fields.Boolean(u'Revisado oxidación')
    r_l_p = fields.Boolean(u'Requiere lijado y pintura')
    r_l_p_photo_before = fields.Binary(u'Foto antes')
    r_l_p_photo_before_name = fields.Char(u'Foto antes')
    r_l_p_photo_after = fields.Binary(u'Foto después')
    r_l_p_photo_after_name = fields.Char(u'Foto después')
    e_d_f = fields.Boolean(u'Existen daños físicos a reparar')
    e_d_f_photo_before = fields.Binary(u'Foto antes')
    e_d_f_photo_before_name = fields.Char(u'Foto antes')
    e_d_f_photo_after = fields.Binary(u'Foto después')
    e_d_f_photo_after_name = fields.Char(u'Foto después')
    b_p_f = fields.Boolean(u'Base presenta fisura')
    m_bds = fields.Boolean(u'Medida - Base diámetro superior - mm')
    m_bdi = fields.Boolean(u'Medida - Base diámetro inferior - mm')
    m_pb = fields.Boolean(u'Medida placa base - mm')
    i_v_p = fields.Boolean(u'Identificador visible pintado')
    b_s_a = fields.Boolean(u'Base con sujeción adecuada')

    arm_type = fields.Selection(ARM_POSITION, string=u'Tipo')
    arm_photo_before = fields.Binary(u'Foto antes')
    arm_photo_before_name = fields.Char()
    arm_photo_after = fields.Binary(u'Foto después')
    arm_photo_after_name = fields.Char()
    s_ad = fields.Boolean(u'Sujeción adecuada')
    a_i = fields.Boolean(u'Ajuste de inclinación')
    a_p = fields.Boolean(u'Ajuste de pernos')
    r_l_pi = fields.Boolean(u'Requiere lijado y pintura')
    r_l_pi_photo_before = fields.Binary(u'Foto antes')
    r_l_pi_photo_before_name = fields.Char(u'Foto antes')
    r_l_pi_photo_after = fields.Binary(u'Foto después')
    r_l_pi_photo_after_name = fields.Char(u'Foto después')
    r_e = fields.Boolean(u'Requiere enderezado')
    r_e_photo_before = fields.Binary(u'Foto antes')
    r_e_photo_before_name = fields.Char(u'Foto antes')
    r_e_photo_after = fields.Binary(u'Foto después')
    r_e_photo_after_name = fields.Char(u'Foto después')

    # SERIALES
    serial_fot = fields.Char(u'Fot')
    serial_tran_d = fields.Char(u'Supresor Datos')
    serial_tran_e = fields.Char(u'Supresor Electrico')
    serial_ups = fields.Char(u'UPS')
    serial_fot_a = fields.Char(u'Fot')
    serial_tran_d_a = fields.Char(u'Supresor Datos')
    serial_tran_e_a = fields.Char(u'Supresor Electrico')
    serial_ups_a = fields.Char(u'UPS')

    # TRABAJO REALIZADO
    solution_detail = fields.Text(u'Detalle de solución')
    observation = fields.Text(u'Observaciones')

    # campos de control
    sent = fields.Boolean('Enviado', track_visibility='onchange')
    received = fields.Boolean('Recibido', track_visibility='onchange')

    img_info = fields.Text()

    @api.multi
    @api.constrains('stage_id')
    def check_stage(self):
        for record in self:
            if record.stage_id:
                if self.env.ref(
                        'helpdesk.group_helpdesk_technical') in self.env.user.groups_id and record.stage_id.sequence > 2:
                    raise ValidationError('No estas autorizado para a cambiar a %s' % record.stage_id.name)

    @api.model
    def create(self, vals):
        vals['name'] = self.env['ir.sequence'].next_by_code('helpdesk.relocation')
        if vals.get('point_id'):
            point = self.env['helpdesk.point'].browse(vals.get('point_id'))
            vals['delegate_id'] = point.center_id.delegate_id.id
            vals['latitude'] = point.latitude
            vals['longitude'] = point.longitude
            vals['address'] = point.address
            vals['state_id']= point.state_id.id
            vals['city_id'] = point.city_id.id
            vals['address_reference'] = point.reference
            vals['box_type'] = point.box_type
            vals['poste_type'] = point.poste_type
            vals['arm_type'] = point.arm_type
            vals['serial_fot_a'] = point.serial_fot
            vals['serial_tran_d_a'] = point.serial_tran_d
            vals['serial_tran_e_a'] = point.serial_tran_e
            vals['serial_ups_a'] = point.serial_ups
            vals['delegate_phone'] = point.center_id.personal_number
            corrective_camera_list = []
            for camera in point.camera_ids:
                corrective_camera_list.append((0,0,{'camera_id':camera.id}))
            vals['relocation_camera_ids'] = corrective_camera_list
        relocation = super(HelpdeskRelocation, self).create(vals)
        if 'technical_id' in vals:
            tmpl = self.env.ref('helpdesk.email_asign_relocation')
            mail_id = tmpl.send_mail(relocation.id)
            mail = self.env['mail.mail'].browse(mail_id)
            mail.send(raise_exception=True)
        return relocation

    @api.multi
    def write(self, vals):
        point_id = vals.get('point_id',self.point_id.id)
        name = vals.get('name', self.name)
        if point_id:
            point = self.env['helpdesk.point'].browse(point_id)
            point_values = {}
            if vals.get('latitude_after'):
                point_values['latitude'] = vals['latitude_after']
            if vals.get('longitude_after'):
                point_values['longitude'] = vals['longitude_after']
            if vals.get('address'):
                point_values['address'] = vals['address']
            if vals.get('state_id'):
                point_values['state_id'] = vals['state_id']
            if vals.get('city_id'):
                point_values['city_id'] = vals['city_id']
            if vals.get('address_reference'):
                point_values['reference'] = vals['address_reference']
            if vals.get('box_type'):
                point_values['box_type'] = vals['box_type']
            if vals.get('poste_type'):
                point_values['poste_type'] = vals['poste_type']
            if vals.get('arm_type'):
                point_values['arm_type'] = vals['arm_type']

            # Seriales
            if vals.get('serial_fot'):
                point_values['serial_fot'] = vals['serial_fot']
            if vals.get('serial_tran_d'):
                point_values['serial_tran_d'] = vals['serial_tran_d']
            if vals.get('serial_tran_e'):
                point_values['serial_tran_e'] = vals['serial_tran_e']
            if vals.get('serial_ups'):
                point_values['serial_ups'] = vals['serial_ups']
            point.sudo().write(point_values)

            # cambiar camaras cuando cambian punto
            if vals.get('point_id'):
                point = self.env['helpdesk.point'].browse(vals.get('point_id'))
                vals['latitude'] = point.latitude
                vals['longitude'] = point.longitude
                vals['address'] = point.address
                vals['state_id'] = point.state_id.id
                vals['city_id'] = point.city_id.id
                vals['address_reference'] = point.reference
                vals['box_type'] = point.box_type
                vals['poste_type'] = point.poste_type
                vals['arm_type'] = point.arm_type
                vals['serial_fot_a'] = point.serial_fot
                vals['serial_tran_d_a'] = point.serial_tran_d
                vals['serial_tran_e_a'] = point.serial_tran_e
                vals['serial_ups_a'] = point.serial_ups
                vals['delegate_phone'] = point.center_id.personal_number
                relocation_camera_ids = []
                camera_ids = []
                for camera in self.relocation_camera_ids:
                    if not camera.camera_id.point_id.id == point_id:
                        relocation_camera_ids.append((2, camera.id,))
                    else:
                        camera_ids.append(camera.camera_id.id)
                for camera in point.camera_ids:
                    if camera.id not in camera_ids:
                        relocation_camera_ids.append((0, 0, {'camera_id': camera.id}))
                vals['relocation_camera_ids'] = relocation_camera_ids

        # tratamiento de imagenes
        img_info = ''
        # diagnostic_camera
        diagnostic_camera = vals.get('diagnostic_camera', self.diagnostic_camera)
        diagnostic_camera_name = vals.get('diagnostic_camera_name', self.diagnostic_camera_name)
        img_info, vals['diagnostic_camera_name'] = self._img_batch(diagnostic_camera, diagnostic_camera_name, img_info
                                                                  , "_cambio_camara.", "Cambio Camara: ", name)
        # diagnostic_poste
        diagnostic_poste = vals.get('diagnostic_poste', self.diagnostic_poste)
        diagnostic_poste_name = vals.get('diagnostic_poste_name', self.diagnostic_poste_name)
        img_info, vals['diagnostic_poste_name'] = self._img_batch(diagnostic_poste, diagnostic_poste_name, img_info
                                                                  , "_cambio_poste.", "Cambio Poste: ", name)
        # diagnostic_arm
        diagnostic_arm = vals.get('diagnostic_arm', self.diagnostic_arm)
        diagnostic_arm_name = vals.get('diagnostic_arm_name', self.diagnostic_arm_name)
        img_info, vals['diagnostic_arm_name'] = self._img_batch(diagnostic_arm, diagnostic_arm_name, img_info
                                                                  , "_cambio_brazo.", "Cambio Brazo: ", name)
        # diagnostic_box
        diagnostic_box = vals.get('diagnostic_box', self.diagnostic_box)
        diagnostic_box_name = vals.get('diagnostic_box_name', self.diagnostic_box_name)
        img_info, vals['diagnostic_box_name'] = self._img_batch(diagnostic_box, diagnostic_box_name, img_info
                                                                  , "_cambio_caja.", "Cambio Caja: ", name)

        # image_pin_response_photo
        image_pin_response_photo = vals.get('image_pin_response_photo', self.image_pin_response_photo)
        image_pin_response_photo_name = vals.get('image_pin_response_photo_name', self.image_pin_response_photo_name)
        img_info, vals['image_pin_response_photo_name'] = self._img_batch(image_pin_response_photo, image_pin_response_photo_name, img_info
                                                                , "_pin_camara.", "Pin Camara: ", name)


        vals['img_info'] = img_info

        relocation = super(HelpdeskRelocation, self).write(vals)
        if 'technical_id' in vals:
            tmpl = self.env.ref('helpdesk.email_asign_relocation')
            mail_id = tmpl.send_mail(self.id)
            mail = self.env['mail.mail'].browse(mail_id)
            mail.send(raise_exception=True)
        return relocation

    @api.model
    def set_coordenate(self, id, latitude, longitude):
        if not id:
            raise ValidationError("Primero debes Guardar el Formulario")
        corrective = self.browse(id)
        corrective.latitude_after = latitude
        corrective.longitude_after = longitude

    def button_dummy(self):
        pass

    @api.model
    def _read_group_stage_ids(self, stages, domain, order):
        search_domain = [('sequence', '>=', 0)]
        stage_ids = stages._search(search_domain, order=order, access_rights_uid=SUPERUSER_ID)
        return stages.browse(stage_ids)


    def _img_batch(self,image,img_name,img_info,prefix_name,prefix_info,name):
        new_name=""
        if image:
            img = ImageMetaData.ImageMetaData(cStringIO.StringIO(base64.b64decode(image)))
            img_info += prefix_info + str(img.get_lat_lng()[0]) +" "+ str(img.get_lat_lng()[1])+"\n"
            if img_name:
                if len(img_name.split('.'))>1:
                    new_name = name + prefix_name +img_name.split('.')[-1]
        return img_info, new_name
