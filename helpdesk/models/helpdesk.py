# -*- coding: utf-8 -*-

from odoo import api, fields, models, SUPERUSER_ID
from odoo.exceptions import ValidationError

from odoo.tools.translate import _
from datetime import datetime

ATTETION_TIME = [('49', 'More 48'),
                 ('48', 'Between 24 and 48'),
                 ('24', 'Less 24')]


class HelpdeskTicket(models.Model):
    _name = "helpdesk.ticket"
    _description = "Helpdesk Ticket"
    _order = "id desc"
    _inherit = ['mail.thread']

    def _default_probability(self):
        stage_id = self._default_stage_id()
        if stage_id:
            return self.env['helpdesk.stage'].browse(stage_id).probability
        return 10

    # def _default_stage_id(self):
    #     team = self.env['helpdesk.team'].sudo()._get_default_team_id(user_id=self.env.uid)
    #     return self._stage_find(team_id=team.id, domain=[('fold', '=', False)]).id

    def _default_stage_id(self):
        search_domain = [('sequence', '>=', 0)]
        return self.env['helpdesk.stage'].search(search_domain, order='sequence', limit=1)

    def _get_delgate_domain(self):
        id = self.env.ref('helpdesk.group_helpdesk_delegate').id
        return [('groups_id', '=', id)]

    name = fields.Char('Id', readonly=True)
    active = fields.Boolean(default=True)
    warranty = fields.Boolean(string=u'Garantía', track_visibility='onchange')
    created_date = fields.Datetime('Create Date', copy=False, track_visibility='onchange')
    date_closed = fields.Datetime('Closed Date', copy=False, track_visibility='onchange')
    date_open = fields.Datetime('Assigned', readonly=True, track_visibility='onchange')
    center_id = fields.Many2one('helpdesk.center', string='Centro', store=True, track_visibility='onchange')
    point_id = fields.Many2one('helpdesk.point', string='Punto', domain="[('center_id','=',center_id)]",
                               ondelete='restrict', track_visibility='onchange')
    delegate_id = fields.Many2one('res.users', string='Delegado', track_visibility='onchange',
                                  default=lambda self: self.env.user.id, ondelete='restrict',
                                  domain=_get_delgate_domain)
    priority = fields.Selection([('0', 'Baja'), ('1', 'Normal'), ('2', 'Media'), ('3', 'Alta')], 'Prioridad',
                                default='0', track_visibility='onchange')
    description = fields.Text('Description', required=True, track_visibility='onchange')
    service_client = fields.Text(u'Persona que solicita el servicio', track_visibility='onchange')
    diagnostic = fields.Text(u'Diagnóstico', track_visibility='onchange')
    ticket_type_id = fields.Many2one('helpdesk.ticket.type', string='Ticket type',
                                     default=lambda self: self.env.ref('helpdesk.type_fix'),
                                     track_visibility='onchange')
    ticket_type_id_code = fields.Char(related='ticket_type_id.code', readonly='1')
    tag_ids = fields.Many2many('helpdesk.tag', string='Tags')
    stage_id = fields.Many2one('helpdesk.stage', string='Stage', track_visibility='onchange', index=True,
                               default=_default_stage_id, group_expand='_read_group_stage_ids')
    day_open = fields.Float(compute='_compute_day_open', string='Days to Assign', store=True)
    day_close = fields.Float(compute='_compute_day_close', string='Days to Close', store=True)
    date_last_stage_update = fields.Datetime(string='Last Stage Update', index=True, default=fields.Datetime.now)
    resolved = fields.Text('Resuelto')
    is_close = fields.Boolean('Closing Kanban stages', help='Tickets in this stages are considered as done.',
                              readonly=True)
    attention_time = fields.Selection(ATTETION_TIME, compute='_compute_attention_time', string="Attention Time",
                                      store=True)
    month = fields.Char(compute='_compute_month_year', store=True, string="Mes")
    year = fields.Char(compute='_compute_month_year', store=True, string=u"Año")
    color = fields.Char(compute='_compute_color')
    short_description = fields.Char(compute='_compute_short_description')
    preventive_ids = fields.One2many('helpdesk.preventive', inverse_name='ticket_id', track_visibility='onchange')

    camera_maintenance_id = fields.Many2one('helpdesk.camera.maintenance', string=u"Reparación de cámara",
                                            ondelete="cascade", track_visibility='onchange')
    poste_maintenance_id = fields.Many2one('helpdesk.poste.maintenance', string=u"Reparación de poste",
                                           ondelete="restrict", track_visibility='onchange')
    relocation_id = fields.Many2one('helpdesk.relocation', string=u"Reubicación de punto",
                                    ondelete="restrict", track_visibility='onchange')

    corrective_cabienet_id = fields.Many2one('helpdesk.corrective.cabinet', string=u"Correctivo Gabinete",
                                             ondelete="restrict", track_visibility='onchange')
    camera_id = fields.Many2one('helpdesk.camera', string=u"Cámara", domain="[('point_id','=',point_id)]",
                                ondelete="restrict", track_visibility='onchange')

    preventive_count = fields.Integer(compute="compute_count")
    corrective_count = fields.Integer(compute="compute_count")

    @api.multi
    def compute_count(self):
        for record in self:
            record.corrective_count = self.env['helpdesk.corrective'].search_count([('ticket_id', '=', record.id)])
            record.preventive_count = self.env['helpdesk.preventive'].search_count([('ticket_id', '=', record.id)])

    def open_corrective(self):
        action = self.env.ref('helpdesk.helpdesk_corrective_action')
        result = action.read()[0]
        result['context'] = {
            'search_default_ticket_id': self.id,
        }
        return result

    def open_preventive(self):
        action = self.env.ref('helpdesk.helpdesk_preventive_action')
        result = action.read()[0]
        result['context'] = {
            'search_default_ticket_id': self.id,
        }
        return result

    @api.multi
    @api.onchange('ticket_type_id')
    def _onchange_ticket_type_id(self):
        for record in self:
            if record.ticket_type_id:
                user_list = []
                for user in record.ticket_type_id.user_ids:
                    user_list.append(user.id)
                return {'domain': {'assign_id': [('id', 'in', user_list)]}}

    @api.multi
    @api.onchange('point_id')
    def _onchange_point_id(self):
        for record in self:
            if record.point_id:
                record.center_id = record.point_id.center_id.id

    @api.multi
    @api.onchange('delegate_id')
    def _onchange_delegate_id(self):
        for record in self:
            if record.delegate_id:
                center = self.env['helpdesk.center'].search([('delegate_id', '=', record.delegate_id.id)], limit=1)
                record.center_id = center.id

    @api.model
    def _onchange_stage_id_values(self, stage_id):
        """ returns the new values when stage_id has changed """

        if not stage_id:
            return {}
        stage = self.env['helpdesk.stage'].browse(stage_id)
        if stage.is_close:
            # self.message_post(body='hola mundo', subject='Ticket cerrado', message_type='email')
            # soporte @ conexiontotal.net
            message = _("<strong>Ticket cerrado</strong><br><br>"
                        "Descripción: %s<br>"
                        "Cliente: %s<br>"
                        "Solución: %s") % (self.description, self.partner_id.name, self.resolved)
            self.message_post_with_template("", body=message)
        return {'is_close': stage.is_close}

    @api.onchange('stage_id')
    def _onchange_stage_id(self):
        values = self._onchange_stage_id_values(self.stage_id.id)
        self.update(values)

    @api.multi
    def _compute_month_year(self):
        for record in self:
            if record.created_date:
                record.month = fields.Datetime.from_string(record.created_date).month
                record.year = fields.Datetime.from_string(record.created_date).year

    @api.multi
    @api.depends('date_closed')
    def _compute_attention_time(self):
        for record in self:
            if record.date_closed:
                if not record.created_date:
                    raise ValidationError(u"Ingresar fecha de creación")
                else:
                    if record.date_closed:
                        date_closed = fields.Datetime.from_string(record.date_closed)
                        created_date = fields.Datetime.from_string(record.created_date)
                        total_hours = (date_closed - created_date).total_seconds() / 60 / 60
                        if total_hours < 0:
                            raise ValidationError("Fecha de cierre debe ser mayor a fecha de apertura")
                        if total_hours < 24:
                            record.attention_time = '24'
                        if total_hours >= 24 and total_hours <= 48:
                            record.attention_time = '48'
                        if total_hours > 48:
                            record.attention_time = '49'

    @api.depends('date_open')
    def _compute_day_open(self):
        """ Compute difference between create date and open date """
        for lead in self.filtered(lambda l: l.date_open):
            if lead.created_date and lead.date_closed:
                date_create = fields.Datetime.from_string(lead.created_date)
                date_open = fields.Datetime.from_string(lead.date_open)
                lead.day_open = abs((date_open - date_create).days)

    @api.depends('date_closed')
    def _compute_day_close(self):
        """ Compute difference between current date and log date """
        for lead in self.filtered(lambda l: l.date_closed):
            if lead.created_date and lead.date_closed:
                date_create = fields.Datetime.from_string(lead.created_date)
                date_close = fields.Datetime.from_string(lead.date_closed)
                lead.day_close = abs((date_close - date_create).days)

    @api.depends('created_date', 'stage_id')
    def _compute_color(self):
        for record in self:
            if record.created_date:
                date_now = fields.Datetime.from_string(fields.Datetime.now())
                created_date = fields.Datetime.from_string(record.created_date)
                total_hours = (date_now - created_date).total_seconds() / 60 / 60
                if total_hours < 24:
                    record.color = '#77DD77'  # green
                if total_hours >= 24 and total_hours <= 48:
                    record.color = '#E9BD15'  # yellow
                if total_hours > 48:
                    record.color = '#FF6961'  # rojo
            if record.stage_id.is_close:
                record.color = '#CFCFC4'  # gray

    @api.depends('description')
    def _compute_short_description(self):
        for record in self:
            record.short_description = record.description[0:40]

    @api.model
    def _read_group_stage_ids(self, stages, domain, order):
        search_domain = [('sequence', '>=', 0)]
        stage_ids = stages._search(search_domain, order=order, access_rights_uid=SUPERUSER_ID)
        return stages.browse(stage_ids)

    # ----------------------------------------
    # ORM override (CRUD, fields_view_get, ...)
    # ----------------------------------------
    @api.model
    def create(self, vals):
        vals['name'] = self.env['ir.sequence'].next_by_code('helpdesk.ticket')
        if vals.get('point_id') and 'point_id' not in vals:
            point = self.env['helpdesk.point'].browse(vals.get('point_id'))
            vals['latitude'] = point.latitude
            vals['longitude'] = point.longitude
            vals['center_id'] = point.longitude
        if vals.get('point_id'):
            point = self.env['helpdesk.point'].browse(vals.get('point_id'))
            vals['center_id'] = point.center_id.id
        # if vals.get('ticket_type_id') == self.env.ref('helpdesk.type_camera_repair').id:
        #     vals_camera_maintenance = {
        #         'point_id':vals.get('point_id'),
        #         'requesting_person': vals.get('service_client'),
        #         'reported_problem': vals.get('description'),
        #         'preview_diagnostic': vals.get('diagnostic'),
        #         'camera_id': vals.get('camera_id'),
        #     }
        #     vals['camera_maintenance_id'] = self.env['helpdesk.camera.maintenance'].sudo().create().id
        ticket = super(HelpdeskTicket, self).create(vals)

        # tickets correctivos
        if ticket.ticket_type_id.name == 'Correctivo':
            vals_corrective = {
                'point_id': ticket.point_id.id,
                'ticket_id': ticket.id,
                'service_client': ticket.service_client,
                'reported_problem': ticket.description,
                'diagnostic': ticket.diagnostic,
                'warranty': ticket.warranty,
            }
            corrective = self.env['helpdesk.corrective'].sudo().create(vals_corrective)

        # tickets correctivos gabinete
        if ticket.ticket_type_id.id == self.env.ref('helpdesk.type_fix_cabinet').id:
            vals_corrective = {
                'point_id': ticket.point_id.id,
                'ticket_id': ticket.id,
                'service_client': ticket.service_client,
                'reported_problem': ticket.description,
                'diagnostic': ticket.diagnostic,
                'warranty': ticket.warranty,
            }
            corrective_cabienet_id = self.env['helpdesk.corrective.cabinet'].sudo().create(vals_corrective).id
            ticket.sudo().write({'corrective_cabienet_id': corrective_cabienet_id})

        # reparacion de camaras//
        if ticket.ticket_type_id.id == self.env.ref('helpdesk.type_camera_repair').id:
            vals_camera_maintenance = {
                'ticket_id': ticket.id,
                'center_id': ticket.center_id.id,
            }
            camera_maintenance_id = self.env['helpdesk.camera.maintenance'].sudo().create(vals_camera_maintenance).id
            ticket.sudo().write({'camera_maintenance_id': camera_maintenance_id})
            ticket.camera_maintenance_id.sudo().write(vals_camera_maintenance)

        # reparacion de poste
        if ticket.ticket_type_id.id == self.env.ref('helpdesk.type_poste_reapir').id:
            vals_poste_maintenance = {
                'ticket_id': ticket.id,
                'center_id': ticket.center_id.id,
                'point_id': ticket.point_id.id,
            }
            poste_maintenance_id = self.env['helpdesk.poste.maintenance'].sudo().create(vals_poste_maintenance).id
            ticket.sudo().write({'poste_maintenance_id': poste_maintenance_id})

        # tickets reubicacoion de punto
        if ticket.ticket_type_id.id == self.env.ref('helpdesk.type_relocation').id:
            vals_corrective = {
                'point_id': ticket.point_id.id,
                'ticket_id': ticket.id,
            }
            relocation_id = self.env['helpdesk.relocation'].sudo().create(vals_corrective).id
            ticket.sudo().write({'relocation_id': relocation_id})

        # notificacion via email
        tmpl = self.env.ref('helpdesk.email_new_ticket')
        mail_id = tmpl.send_mail(ticket.id)
        mail = self.env['mail.mail'].browse(mail_id)
        # mail.send()
        mail.send(raise_exception=True)
        return ticket

    @api.multi
    def write(self, vals):
        if self.env.ref('helpdesk.group_helpdesk_technical') in self.env.user.groups_id and vals.get('stage_id'):
            return super(HelpdeskTicket, self).sudo().write(vals)
        return super(HelpdeskTicket, self).write(vals)
