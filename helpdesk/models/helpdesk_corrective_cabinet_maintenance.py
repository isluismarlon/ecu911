# -*- coding: utf-8 -*-

from odoo import fields, models, api

CAMERA_TYPE = [
    ('domo_hd', u'DOMO HD'),
    ('domo_sd', u'DOMO SD'),
    ('fixed', u'FIJA'),
    ('long', u'LARGO'),
    ('aperture', u'ALCANCE'),
    ('laser', u'LASER'),
    ('plate', u'PLACAS'),
    ('thermal', u'TERMICA'),
    ('mini', u'MINIDOMO'),
    ('speeddome', u'SPEEDDOME')
]

BRAND = [
    ('huawei', u'Huawei'),
    ('tiandy', u'Tiandy'),
    ('ceiec', u'Ceiec'),
    ('flir', u'Flir'),
    ('znv', u'ZNV'),
    ('visualint', u'Visualint'),
    ('vidicam',u'Vidicam'),
]


class HelpdeskCorrectiveCabinetCamera(models.Model):
    _name = 'helpdesk.corrective.cabinet.camera'

    corrective_id = fields.Many2one('helpdesk.corrective.cabinet')
    camera_id = fields.Many2one('helpdesk.camera', string=u'Cámara', readonly=True)
    ip_address = fields.Char(string=u'IP')
    type = fields.Selection(CAMERA_TYPE,string=u'Tipo')
    serial_1 = fields.Char(string=u'Serial 1 Nuevo')
    serial_1_a = fields.Char(string=u'Serial 1 Anterior')
    serial_2 = fields.Char(string=u'Serial 2 Nuevo')
    serial_2_a = fields.Char(string=u'Serial 2 Anterior')
    brand = fields.Selection(BRAND, string="Marca")
    model = fields.Char(string="Modelo")
    d_u_a = fields.Boolean(string=u'Domo ubicado y asegurado')
    t_c_a = fields.Boolean(string=u'Tapa de cámara asegurada')
    c_a = fields.Boolean(string=u'Cámara asegurada')
    c_b_h = fields.Boolean(string=u'Cables bajo herraje')
    c_e = fields.Boolean(string=u'Cámara energizada')
    p_v_v = fields.Boolean(string=u'Prueba con video vigilancia')
    f_v = fields.Boolean(string=u'Filtraciones verificadas')
    v_a_e = fields.Boolean(string=u'Voltaje adecuado entrada')
    v_a_s = fields.Boolean(string=u'Voltaje adecuado Salida')

    def report_camera_1(self):
        text_array = []
        pdf = ""
        if self.d_u_a:
            text_array.append(u"Domo ubicado y asegurado")
        if self.t_c_a:
            text_array.append(u"Tapa de camara asegurada")
        if self.c_a:
            text_array.append(u"Cámara asegurada")
        if self.c_b_h:
            text_array.append(u"Cables bajo herraje")
        if self.c_e:
            text_array.append(u"Cámara energizada")
        return text_array

    def report_camera_2(self):
        text_array = []
        pdf = ""
        if self.p_v_v:
            text_array.append(u"Prueba con video vigilancia")
        if self.f_v:
            text_array.append(u"Filtraciones verificadas")
        if self.v_a_e:
            text_array.append(u"Voltaje adecuado entrada")
        if self.v_a_s:
            text_array.append(u"Voltaje adecuado salida")
        return text_array

    # def report_pdf(self):
    #     text_array = []
    #     pdf = ""
    #     text_array.append(u"Tapa de camara asegurada   \u2713 \u0009 \u000A")
    #     text_array.append("Camara asegurada        ")
    #     text_array.append("Tapa de camara asegurada")
    #     for index in range(0, len(text_array), 2):
    #         if index + 1 < len(text_array):
    #             pdf += text_array[index] + "\t\t\t" + text_array[index + 1] + u"\u0009 \u000A"
    #         else:
    #             pdf += text_array[index] + "\n"
    #     return pdf
    @api.model
    def create(self, vals):
        if vals.get('camera_id'):
            camera = self.env['helpdesk.camera'].browse(vals.get('camera_id'))
            vals['ip_address'] = camera.ip_number
            vals['type'] = camera.camera_type
            vals['serial_1'] = camera.serial_1
            vals['serial_1_a'] = camera.serial_1
            vals['serial_2'] = camera.serial_2
            vals['serial_2_a'] = camera.serial_2
            vals['brand'] = camera.brand
            vals['model'] = camera.model
        return super(HelpdeskCorrectiveCabinetCamera, self).create(vals)

    @api.multi
    def write(self, vals):
        camera_id = vals.get('camera_id', self.camera_id.id)
        if camera_id:
            camera = self.env['helpdesk.camera'].browse(camera_id)
            camera_values = {}
            if vals.get('ip_address'):
                camera_values['ip_number'] = vals['ip_address']
            if vals.get('type'):
                camera_values['camera_type'] = vals['type']
            if vals.get('serial_1'):
                camera_values['serial_1'] = vals['serial_1']
            if vals.get('serial_2'):
                camera_values['serial_2'] = vals['serial_2']
            if vals.get('brand'):
                camera_values['brand'] = vals['brand']
            if vals.get('model'):
                camera_values['model'] = vals['model']
            camera.sudo().write(camera_values)
        return super(HelpdeskCorrectiveCabinetCamera, self).write(vals)

