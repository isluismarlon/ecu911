# -*- coding: utf-8 -*-

from odoo import fields, models, api
import base64
import cStringIO
from . import ImageMetaData


class Image(models.Model):

    _name = "helpdesk.image"

    name = fields.Char('Name', required=True)
    photo_before = fields.Binary(u'Foto antes')
    photo_before_name = fields.Char()
    photo_after = fields.Binary(u'Foto después')
    photo_after_name = fields.Char()
    info = fields.Char(compute='_compute_info', store=True)

    # modelos asociados
    camera_maintenance_id = fields.Many2one('helpdesk.camera.maintenance')

    # @api.multi
    # @api.depends('image')
    # def _compute_info(self):
    #     for record in self:
    #         if record.image:
    #             img = ImageMetaData.ImageMetaData(cStringIO.StringIO(base64.b64decode(record.image)))
    #             record.info = str(img.get_lat_lng()[0]) + " " + str(img.get_lat_lng()[1]) + "\n"

