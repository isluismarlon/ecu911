# -*- coding: utf-8 -*-

from odoo import fields, models, api
import base64
import cStringIO
from . import ImageMetaData


class HelpdeskCorrectiveRepairRegistry(models.Model):
    _name = 'helpdesk.corrective.repair.registry'

    corrective_id = fields.Many2one('helpdesk.corrective')
    name = fields.Many2one('helpdesk.item', string=u'Repuesto')
    qty = fields.Float('Cantidad', digits=(16,2), default=1.0)
    # name = fields.Char()
    photo_before = fields.Binary(u'Foto antes')
    photo_before_name = fields.Char()
    photo_after = fields.Binary(u'Foto después')
    photo_after_name = fields.Char()
    img_info = fields.Text()

    @api.model
    def create(self, vals):
        corrective_item_id = vals.get('name', self.name.id)
        name = self.env['helpdesk.item'].browse(corrective_item_id).name
        photo_name = ''.join(e for e in name if e.isalnum())
        corrective_id = vals.get('corrective_id', self.corrective_id.id)
        corrective_name = ""
        if corrective_id:
            corrective_name = self.env['helpdesk.corrective'].browse(corrective_id).name

        # tratamiento de imagenes
        img_info = ''
        # foto caja antes
        photo_before = vals.get('photo_before', self.photo_before)
        photo_before_name = vals.get('photo_before_name', self.photo_before_name)
        img_info, vals['photo_before_name'] = self._img_batch(photo_before, photo_before_name, img_info
                                                              , "_" + photo_name + "_antes.", name + " antes : ",
                                                              corrective_name)
        # foto caja despues
        photo_after = vals.get('photo_after', self.photo_after)
        photo_after_name = vals.get('photo_after_name', self.photo_after_name)
        img_info, vals['photo_after_name'] = self._img_batch(photo_after, photo_after_name, img_info
                                                             , "_" + photo_name + "_despues.", name + " despues : ",
                                                             corrective_name)

        vals['img_info'] = img_info
        return super(HelpdeskCorrectiveRepairRegistry, self).create(vals)

    @api.multi
    def write(self, vals):
        corrective_item_id = vals.get('name', self.name.id)
        name = self.env['helpdesk.item'].browse(corrective_item_id).name
        photo_name = ''.join(e for e in name if e.isalnum())
        corrective_id = vals.get('corrective_id',self.corrective_id.id)
        corrective_name = ""
        if corrective_id:
            corrective_name = self.env['helpdesk.corrective'].browse(corrective_id).name


        # tratamiento de imagenes
        img_info = ''
        # foto caja antes
        photo_before = vals.get('photo_before', self.photo_before)
        photo_before_name = vals.get('photo_before_name', self.photo_before_name)
        img_info, vals['photo_before_name'] = self._img_batch(photo_before, photo_before_name, img_info
                                                              , "_"+photo_name+"_antes.", name+" antes : ", corrective_name)
        # foto caja despues
        photo_after = vals.get('photo_after', self.photo_after)
        photo_after_name = vals.get('photo_after_name', self.photo_after_name)
        img_info, vals['photo_after_name'] = self._img_batch(photo_after, photo_after_name, img_info
                                                        , "_"+photo_name + "_despues.", name + " despues : ", corrective_name)

        vals['img_info'] = img_info
        return super(HelpdeskCorrectiveRepairRegistry, self).write(vals)


    def _img_batch(self,image,img_name,img_info,prefix_name,prefix_info,name):
        new_name=""
        if image:
            img = ImageMetaData.ImageMetaData(cStringIO.StringIO(base64.b64decode(image)))
            img_info += prefix_info + str(img.get_lat_lng()[0]) +" "+ str(img.get_lat_lng()[1])+"\n"
            if img_name:
                if len(img_name.split('.'))>1:
                    new_name = name + prefix_name +img_name.split('.')[-1]
        return img_info, new_name

class HelpdeskCorrectiveCabinetRepairRegistry(models.Model):
    _name = 'helpdesk.corrective.cabinet.repair.registry'

    corrective_cabinet_id = fields.Many2one('helpdesk.corrective.cabinet')
    name = fields.Many2one('helpdesk.item', string=u'Repuesto')
    qty = fields.Float('Cantidad', digits=(16,2), default=1.0)
    # name = fields.Char()
    photo_before = fields.Binary(u'Foto antes')
    photo_before_name = fields.Char()
    photo_after = fields.Binary(u'Foto después')
    photo_after_name = fields.Char()
    img_info = fields.Text()

    @api.model
    def create(self, vals):
        corrective_item_id = vals.get('name', self.name.id)
        name = self.env['helpdesk.item'].browse(corrective_item_id).name
        photo_name = ''.join(e for e in name if e.isalnum())
        corrective_cabinet_id = vals.get('corrective_cabinet_id', self.corrective_cabinet_id.id)
        corrective_cabinet_name = ""
        if corrective_cabinet_id:
            corrective_cabinet_name = self.env['helpdesk.corrective.cabinet'].browse(corrective_cabinet_id).name

        # tratamiento de imagenes
        img_info = ''
        # foto caja antes
        photo_before = vals.get('photo_before', self.photo_before)
        photo_before_name = vals.get('photo_before_name', self.photo_before_name)
        img_info, vals['photo_before_name'] = self._img_batch(photo_before, photo_before_name, img_info
                                                              , "_" + photo_name + "_antes.", name + " antes : ",
                                                              corrective_cabinet_name)
        # foto caja despues
        photo_after = vals.get('photo_after', self.photo_after)
        photo_after_name = vals.get('photo_after_name', self.photo_after_name)
        img_info, vals['photo_after_name'] = self._img_batch(photo_after, photo_after_name, img_info
                                                             , "_" + photo_name + "_despues.", name + " despues : ",
                                                             corrective_cabinet_name)

        vals['img_info'] = img_info
        return super(HelpdeskCorrectiveCabinetRepairRegistry, self).create(vals)

    @api.multi
    def write(self, vals):
        corrective_item_id = vals.get('name', self.name.id)
        name = self.env['helpdesk.item'].browse(corrective_item_id).name
        photo_name = ''.join(e for e in name if e.isalnum())
        corrective_cabinet_id = vals.get('corrective_cabinet_id',self.corrective_cabinet_id.id)
        corrective_cabinet_name = ""
        if corrective_cabinet_id:
            corrective_cabinet_name = self.env['helpdesk.corrective.cabinet'].browse(corrective_cabinet_id).name


        # tratamiento de imagenes
        img_info = ''
        # foto caja antes
        photo_before = vals.get('photo_before', self.photo_before)
        photo_before_name = vals.get('photo_before_name', self.photo_before_name)
        img_info, vals['photo_before_name'] = self._img_batch(photo_before, photo_before_name, img_info
                                                              , "_"+photo_name+"_antes.", name+" antes : ", corrective_cabinet_name)
        # foto caja despues
        photo_after = vals.get('photo_after', self.photo_after)
        photo_after_name = vals.get('photo_after_name', self.photo_after_name)
        img_info, vals['photo_after_name'] = self._img_batch(photo_after, photo_after_name, img_info
                                                        , "_"+photo_name + "_despues.", name + " despues : ", corrective_cabinet_name)

        vals['img_info'] = img_info
        return super(HelpdeskCorrectiveCabinetRepairRegistry, self).write(vals)


    def _img_batch(self,image,img_name,img_info,prefix_name,prefix_info,name):
        new_name=""
        if image:
            img = ImageMetaData.ImageMetaData(cStringIO.StringIO(base64.b64decode(image)))
            img_info += prefix_info + str(img.get_lat_lng()[0]) +" "+ str(img.get_lat_lng()[1])+"\n"
            if img_name:
                if len(img_name.split('.'))>1:
                    new_name = name + prefix_name +img_name.split('.')[-1]
        return img_info, new_name

class HelpdeskPreventiveRepairRegistry(models.Model):
    _name = 'helpdesk.preventive.repair.registry'

    preventive_id = fields.Many2one('helpdesk.preventive')
    name = fields.Many2one('helpdesk.item', string=u'Repuesto')
    qty = fields.Float('Cantidad', digits=(16, 2), default=1.0)
    photo_before = fields.Binary(u'Foto antes')
    photo_before_name = fields.Char()
    photo_after = fields.Binary(u'Foto después')
    photo_after_name = fields.Char()
    img_info = fields.Text()

    api.model
    def create(self, vals):
        preventive_item_id = vals.get('name', self.name.id)
        name = self.env['helpdesk.item'].browse(preventive_item_id).name
        photo_name = ''.join(e for e in name if e.isalnum())
        preventive_id = vals.get('preventive_id', self.preventive_id.id)
        preventive_name = ""
        if preventive_id:
            preventive_name = self.env['helpdesk.preventive'].browse(preventive_id).name

        # tratamiento de imagenes
        img_info = ''
        # foto antes
        photo_before = vals.get('photo_before', self.photo_before)
        photo_before_name = vals.get('photo_before_name', self.photo_before_name)
        img_info, vals['photo_before_name'] = self._img_batch(photo_before, photo_before_name, img_info
                                                              , "_" + photo_name + "_antes.", name + " antes : ",
                                                              preventive_name)
        # foto despues
        photo_after = vals.get('photo_after', self.photo_after)
        photo_after_name = vals.get('photo_after_name', self.photo_after_name)
        img_info, vals['photo_after_name'] = self._img_batch(photo_after, photo_after_name, img_info
                                                             , "_" + photo_name + "_despues.", name + " despues : ",
                                                             preventive_name)

        vals['img_info'] = img_info
        return super(HelpdeskPreventiveRepairRegistry, self).create(vals)


    @api.multi
    def write(self, vals):
        preventive_item_id = vals.get('name', self.name.id)
        name = self.env['helpdesk.item'].browse(preventive_item_id).name
        photo_name = ''.join(e for e in name if e.isalnum())
        preventive_id = vals.get('preventive_id', self.preventive_id.id)
        preventive_name = ""
        if preventive_id:
            preventive_name = self.env['helpdesk.preventive'].browse(preventive_id).name

        # tratamiento de imagenes
        img_info = ''
        # foto caja antes
        photo_before = vals.get('photo_before', self.photo_before)
        photo_before_name = vals.get('photo_before_name', self.photo_before_name)
        img_info, vals['photo_before_name'] = self._img_batch(photo_before, photo_before_name, img_info
                                                              , "_"+photo_name+"_antes.", name+" antes : ", preventive_name)
        # foto caja despues
        photo_after = vals.get('photo_after', self.photo_after)
        photo_after_name = vals.get('photo_after_name', self.photo_after_name)
        img_info, vals['photo_after_name'] = self._img_batch(photo_after, photo_after_name, img_info
                                                        , "_"+photo_name + "_despues.", name + " despues : ", preventive_name)

        vals['img_info'] = img_info
        return super(HelpdeskPreventiveRepairRegistry, self).write(vals)


    def _img_batch(self,image,img_name,img_info,prefix_name,prefix_info,name):
        new_name=""
        if image:
            img = ImageMetaData.ImageMetaData(cStringIO.StringIO(base64.b64decode(image)))
            img_info += prefix_info + str(img.get_lat_lng()[0]) +" "+ str(img.get_lat_lng()[1])+"\n"
            if img_name:
                if len(img_name.split('.'))>1:
                    new_name = name + prefix_name +img_name.split('.')[-1]
        return img_info, new_name
    
class HelpdeskRelocationRepairRegistry(models.Model):
    _name = 'helpdesk.relocation.repair.registry'

    relocation_id = fields.Many2one('helpdesk.relocation')
    name = fields.Many2one('helpdesk.item', string=u'Repuesto')
    qty = fields.Float('Cantidad', digits=(16, 2), default=1.0)
    photo_before = fields.Binary(u'Foto antes')
    photo_before_name = fields.Char()
    photo_after = fields.Binary(u'Foto después')
    photo_after_name = fields.Char()
    img_info = fields.Text()

    api.model
    def create(self, vals):
        relocation_item_id = vals.get('name', self.name.id)
        name = self.env['helpdesk.item'].browse(relocation_item_id).name
        photo_name = ''.join(e for e in name if e.isalnum())
        relocation_id = vals.get('relocation_id', self.relocation_id.id)
        relocation_name = ""
        if relocation_id:
            relocation_name = self.env['helpdesk.relocation'].browse(relocation_id).name

        # tratamiento de imagenes
        img_info = ''
        # foto antes
        photo_before = vals.get('photo_before', self.photo_before)
        photo_before_name = vals.get('photo_before_name', self.photo_before_name)
        img_info, vals['photo_before_name'] = self._img_batch(photo_before, photo_before_name, img_info
                                                              , "_" + photo_name + "_antes.", name + " antes : ",
                                                              relocation_name)
        # foto despues
        photo_after = vals.get('photo_after', self.photo_after)
        photo_after_name = vals.get('photo_after_name', self.photo_after_name)
        img_info, vals['photo_after_name'] = self._img_batch(photo_after, photo_after_name, img_info
                                                             , "_" + photo_name + "_despues.", name + " despues : ",
                                                             relocation_name)

        vals['img_info'] = img_info
        return super(HelpdeskRelocationRepairRegistry, self).create(vals)


    @api.multi
    def write(self, vals):
        relocation_item_id = vals.get('name', self.name.id)
        name = self.env['helpdesk.item'].browse(relocation_item_id).name
        photo_name = ''.join(e for e in name if e.isalnum())
        relocation_id = vals.get('relocation_id', self.relocation_id.id)
        relocation_name = ""
        if relocation_id:
            relocation_name = self.env['helpdesk.relocation'].browse(relocation_id).name

        # tratamiento de imagenes
        img_info = ''
        # foto caja antes
        photo_before = vals.get('photo_before', self.photo_before)
        photo_before_name = vals.get('photo_before_name', self.photo_before_name)
        img_info, vals['photo_before_name'] = self._img_batch(photo_before, photo_before_name, img_info
                                                              , "_"+photo_name+"_antes.", name+" antes : ", relocation_name)
        # foto caja despues
        photo_after = vals.get('photo_after', self.photo_after)
        photo_after_name = vals.get('photo_after_name', self.photo_after_name)
        img_info, vals['photo_after_name'] = self._img_batch(photo_after, photo_after_name, img_info
                                                        , "_"+photo_name + "_despues.", name + " despues : ", relocation_name)

        vals['img_info'] = img_info
        return super(HelpdeskRelocationRepairRegistry, self).write(vals)


    def _img_batch(self,image,img_name,img_info,prefix_name,prefix_info,name):
        new_name=""
        if image:
            img = ImageMetaData.ImageMetaData(cStringIO.StringIO(base64.b64decode(image)))
            img_info += prefix_info + str(img.get_lat_lng()[0]) +" "+ str(img.get_lat_lng()[1])+"\n"
            if img_name:
                if len(img_name.split('.'))>1:
                    new_name = name + prefix_name +img_name.split('.')[-1]
        return img_info, new_name

class HelpdeskItem(models.Model):
    _name = 'helpdesk.item'

    name = fields.Char(u'Nombre del criterio')



