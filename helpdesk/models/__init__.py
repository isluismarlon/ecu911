# -*- coding: utf-8 -*-

from . import helpdesk
from . import helpdesk_stage
from . import helpdesk_tag
from . import helpdesk_point
from . import helpdesk_center
from . import helpdesk_camera
from . import helpdesk_technical
from . import helpdesk_company
from . import helpdesk_ticket_type
from . import helpdesk_preventive
from . import helpdesk_preventive_maintenance
from . import helpdesk_corrective
from . import helpdesk_corrective_maintenance
from . import helpdesk_corrective_cabinet
from . import helpdesk_corrective_cabinet_maintenance
from . import helpdesk_repair_registry
from . import contact_wizard
from . import helpdesk_camera_maintenance
from . import helpdesk_poste_maintenance
from . import helpdesk_image
from . import helpdesk_relocation
from . import helpdesk_relocation_maintenance

