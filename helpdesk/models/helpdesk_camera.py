# -*- coding: utf-8 -*-

from odoo import fields, models, api
from odoo.exceptions import UserError
from odoo.tools.translate import _

CAMERA_TYPE = [
    ('domo_hd', u'DOMO HD'),
    ('domo_sd', u'DOMO SD'),
    ('fixed', u'FIJA'),
    ('long', u'LARGO'),
    ('aperture', u'ALCANCE'),
    ('laser', u'LASER'),
    ('plate', u'PLACAS'),
    ('thermal', u'TERMICA'),
    ('mini', u'MINIDOMO'),
    ('speeddome', u'SPEEDDOME')
]
BRAND = [
    ('huawei', u'Huawei'),
    ('tiandy', u'Tiandy'),
    ('ceiec', u'Ceiec'),
    ('flir', u'Flir'),
    ('znv', u'ZNV'),
    ('visualint', u'Visualint'),
    ('vidicam',u'Vidicam'),
]


class Camera(models.Model):
    _name = 'helpdesk.camera'
    _inherit = ['mail.thread']

    name = fields.Char(string=u'ID', required=True, track_visibility='onchange')
    ip_number = fields.Char(string=u'IP', track_visibility='onchange')
    mask = fields.Char(string=u'Máscara', track_visibility='onchange')
    serial_1 = fields.Char(string=u'Serial 1', track_visibility='onchange')
    serial_2 = fields.Char(string=u'Serial 2', track_visibility='onchange')
    point_id = fields.Many2one('helpdesk.point', string=u'Punto', track_visibility='onchange')
    camera_type = fields.Selection(CAMERA_TYPE, string=u'Tipo', track_visibility='onchange')
    brand = fields.Selection(BRAND, string="Marca", track_visibility='onchange')
    model = fields.Char(string="Modelo", track_visibility='onchange')


    contract_link = fields.Char(string=u'Enlace del contrato', track_visibility='onchange')
    connectivity_details = fields.Char(string=u'Detalles de conectividad', track_visibility='onchange')
    agreetment = fields.Char('Convenio GAD', track_visibility='onchange')
    platform = fields.Char('Plataforma', help=u'Plataforma de visualización', track_visibility='onchange')
    who_pays = fields.Char('Quien Paga', track_visibility='onchange')

