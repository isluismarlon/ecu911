# -*- coding: utf-8 -*-

from odoo import fields, models, api

BOX_TYPE = [
    ('electric_data', u'Eléctrica y datos'),
    ('unique', u'Única')
]

POSTE_TYPE = [
    ('concrete', u'Hormigón'),
    ('metal', u'Metálico'),
    ('fiber', u'Fibra de vidrio'),
    ('no_exist', u'No existe'),
]

ARM_POSITION = [
    ('poste', u'Poste'),
    ('wall', u'Pared')
]


class Point(models.Model):
    _name = "helpdesk.point"
    _inherit = ['mail.thread']

    name = fields.Char('ID')
    state_id = fields.Many2one('res.country.state', string=u'Provincia', track_visibility='onchange')
    city_id = fields.Many2one('res.state.city', string=u'Ciudad', track_visibility='onchange')
    address = fields.Char(string=u'Dirección', track_visibility='onchange')
    latitude = fields.Float(string=u'Latitud', digits=(4,7), track_visibility='onchange')
    longitude = fields.Float(string=u'Longitud', digits=(4,7), track_visibility='onchange')
    center_id = fields.Many2one('helpdesk.center', string=u'Centro', track_visibility='onchange')
    reference = fields.Char(string=u'Referencia', track_visibility='onchange')
    box_type = fields.Selection(BOX_TYPE, string=u'Caja', track_visibility='onchange')
    poste_type = fields.Selection(POSTE_TYPE, string=u'Poste', track_visibility='onchange')
    arm_type = fields.Selection(ARM_POSITION, string=u'Brazo', track_visibility='onchange')
    camera_ids = fields.One2many('helpdesk.camera', string=u'Cámara', inverse_name='point_id', track_visibility='onchange')

    # SERIALES
    serial_fot = fields.Char('Fot')
    serial_tran_d = fields.Char('Supresor Datos')
    serial_tran_e = fields.Char('Supresor Electrico')
    serial_ups = fields.Char('UPS')


    preventive_count = fields.Integer(compute="compute_count")
    corrective_count = fields.Integer(compute="compute_count")

    @api.multi
    def compute_count(self):
        for record in self:
            record.corrective_count = self.env['helpdesk.corrective'].search_count([('point_id','=',record.id)])
            record.preventive_count = self.env['helpdesk.preventive'].search_count([('point_id', '=', record.id)])


    def open_corrective(self):
        action = self.env.ref('helpdesk.helpdesk_corrective_action')
        result = action.read()[0]
        result['context'] = {
                'search_default_point_id': self.id,
            }
        return result

    def open_preventive(self):
        action = self.env.ref('helpdesk.helpdesk_preventive_action')
        result = action.read()[0]
        result['context'] = {
            'search_default_point_id': self.id,
            'default_ticket_id': self.id
        }
        return result



    _sql_constraints = [
        # ('name_uniq', 'unique (ip)', u"Identificador del punto debe ser único"),
        # ('coordenate_uniq','unique ()', u"Puntos de coordenadas deben ser único"),

    ]
