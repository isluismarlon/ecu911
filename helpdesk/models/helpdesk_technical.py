# -*- coding: utf-8 -*-

from odoo import fields, models, api
from odoo.exceptions import UserError
from odoo.tools.translate import _

class Technical(models.Model):
    _name = 'helpdesk.technical'
    _rec_name = 'user_id'

    user_id = fields.Many2one('res.users', string=u'Técnico')
    phone = fields.Char(string=u'Telefono')
    email = fields.Char(related='user_id.email', readonly=True)
    company_id = fields.Many2one('helpdesk.company',string=u"Compañia")

