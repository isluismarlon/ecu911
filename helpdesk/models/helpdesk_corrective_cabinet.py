# -*- coding: utf-8 -*-

from odoo import fields, models, api,SUPERUSER_ID
from odoo.exceptions import ValidationError
# from . import ImageMetaData
from PIL import Image
from PIL.ExifTags import TAGS, GPSTAGS
import base64
import cStringIO
from . import ImageMetaData
from odoo import tools

ARM_POSITION = [
    ('poste', u'Poste'),
    ('wall', u'Pared')
]

POSTE_TYPE = [
    ('concrete', u'Hormigón'),
    ('metal', u'Metálico'),
    ('fiber', u'Fibra de vidrio'),
    ('no_exist', u'No existe'),
]

BOX_TYPE = [
    ('electric_data', u'Eléctrica y datos'),
    ('unique', u'Única')
]

YES_NO = [
    ('yes', 'Si'),
    ('no', 'No')
]


class HelpdeskCorrectiveCabinet(models.Model):
    _name = 'helpdesk.corrective.cabinet'
    _inherit = ['mail.thread']

    def _default_stage_id(self):
        search_domain = [('sequence', '>=', 0)]
        return self.env['helpdesk.stage'].search(search_domain, order='sequence', limit=1)

    # INFORMACIÓN DE PUNTO DE REVISIÓN
    name = fields.Char(string=u'ID', readonly=True)
    active = fields.Boolean(default=True, track_visibility='onchange')
    warranty = fields.Boolean(string=u'Garantía',track_visibility='onchange')
    technical_id = fields.Many2one('helpdesk.technical', string=u'Técnico', track_visibility='onchange')
    stage_id = fields.Many2one('helpdesk.stage', string='Stage', track_visibility='onchange', index=True ,default=_default_stage_id, group_expand='_read_group_stage_ids')
    delegate_id = fields.Many2one('res.users', string=u'Delegado', readonly=True, store=True)
    delegate_phone = fields.Char(string=u'Teléfono del delegado')
    ticket_id = fields.Many2one('helpdesk.ticket', string="Ticket", readonly=True, ondelete='cascade')
    point_id = fields.Many2one('helpdesk.point', string=u'ID Punto')
    center_id = fields.Many2one('helpdesk.center', related='point_id.center_id', readonly=True, string='Centro', store=True)
    address = fields.Char(u'Dirección')
    address_reference = fields.Text(u'Referencia Ubicación')
    date = fields.Date(u'Fecha', default=fields.Date.today())
    state_id = fields.Many2one('res.country.state', string=u'Provincia')
    city_id = fields.Many2one('res.state.city', string=u'Cantón')
    latitude = fields.Float(string=u'Latitud', digits=(4, 7))
    longitude = fields.Float(string=u'Longitud', digits=(4, 7))
    observation = fields.Text(u'Observaciones')

    ecu_user = fields.Char(u'Técnico de ECU911 que realiza la prueba')
    pvo = fields.Selection(YES_NO, string=u'Punto de vigilancia operativo', default='no')

    # INFORMACIÓN DE TRABAJO
    service_client = fields.Text(related='ticket_id.service_client', string=u'Persona que solicita el servicio')
    reported_problem = fields.Text(related='ticket_id.description', string=u'Problema reportado por el cliente')
    diagnostic = fields.Text(related='ticket_id.diagnostic', string=u'Diagnóstico')

    # REGISTRO DE REPUESTOS CAMBIADOS
    registry_ids = fields.One2many('helpdesk.corrective.cabinet.repair.registry', inverse_name='corrective_cabinet_id')

    # MANTENIMIENTO CORRECTIVO DE CÁMARAS
    corrective_camera_ids = fields.One2many('helpdesk.corrective.cabinet.camera', inverse_name='corrective_id')

    #MANTENIMIENTO CORRECTIVO DE CAJAS
    box_type = fields.Selection(BOX_TYPE, string=u'Tipo')
    c_e_e_g = fields.Boolean(string=u'Cajas etiquetada: E-GYE-115')
    u_p_s_e = fields.Boolean(string=u'UPS etiquetado: UPS-1')
    b_e_b = fields.Boolean(string=u'Breakers etiquetados: Breaker-1-10A')
    c_e_e = fields.Boolean(string=u'Cable eléctrico etiquetado: CAM1-DOME')
    t_c_e = fields.Boolean(string=u'Toma corriente etiquetado: Toma-1')
    o_o = fields.Boolean(string=u'ODF: ODF-1')
    f_v = fields.Boolean(string=u'Filtraciones verificadas')
    s_a = fields.Boolean(string=u'Sujeción adecuada')
    b_v = fields.Boolean(string=u'Bisagras verificadas')
    c_v = fields.Boolean(string=u'Cerraduras verificadas')

    # campos de imagenes
    box_photo_before = fields.Binary(u'Foto caja antes')
    box_photo_before_name = fields.Char()
    box_photo_after = fields.Binary(u'Foto caja después')
    box_photo_after_name = fields.Char()
    img_info = fields.Text()

    # MANTENIMIENTO CORRECTIVO DE POSTE/BRAZOS
    poste_type = fields.Selection(POSTE_TYPE,string=u'Tipo')
    poste_n_a = fields.Boolean(string=u'Númeracion aplicada')
    poste_b_s_a = fields.Boolean(string=u'Base con sujeción adecuada')
    poste_l_p_a = fields.Boolean(string=u'Lijado y pintura aplicado')
    poste_m_b_v = fields.Boolean(string=u'Medidas de base verificada')

    arm_type = fields.Selection(ARM_POSITION, string=u'Tipo')
    arm_s_a = fields.Boolean(string=u'Sujeción adecuada')
    arm_l_p_a = fields.Boolean(string=u'Lijado y pintura aplicado')
    arm_i_a = fields.Boolean(string=u'Inclinación adecuada')

    # SERIALES
    serial_fot = fields.Char(u'Fot')
    serial_tran_d = fields.Char(u'Supresor Datos')
    serial_tran_e = fields.Char(u'Supresor Electrico')
    serial_ups = fields.Char(u'UPS')
    serial_fot_a = fields.Char(u'Fot')
    serial_tran_d_a = fields.Char(u'Supresor Datos')
    serial_tran_e_a = fields.Char(u'Supresor Electrico')
    serial_ups_a = fields.Char(u'UPS')

    # campos de control
    sent = fields.Boolean('Enviado', track_visibility='onchange')
    received = fields.Boolean('Recibido',track_visibility='onchange')

    # TRABAJO REALIZADO
    solution_detail = fields.Text(u'Detalle de solución')
    observation = fields.Text(u'Observaciones')

    def report_box(self):
        text_array = []
        pdf = ""
        if self.c_e_e_g:
            text_array.append(u'Cajas etiquetada')
        if self.u_p_s_e:
            text_array.append(u'UPS etiquetado')
        if self.b_e_b:
            text_array.append(u'Breakers etiquetados')
        if self.c_e_e:
            text_array.append(u'Cable eléctrico etiquetado')
        if self.t_c_e:
            text_array.append(u'Toma corriente etiquetado')
        if self.o_o:
            text_array.append(u'ODF: ODF-1')
        if self.f_v:
            text_array.append(u'Filtraciones verificadas')
        if self.s_a:
            text_array.append(u'Sujeción adecuada')
        if self.b_v:
            text_array.append(u'Bisagras verificadas')
        if self.c_v:
            text_array.append(u'Cerraduras verificadas')
        return text_array

    def report_poste(self):
        text_array = []
        pdf = ""
        if self.poste_n_a:
            text_array.append(u'Númeracion aplicada')
        if self.poste_b_s_a:
            text_array.append(u'Base con sujeción adecuada')
        if self.poste_l_p_a:
            text_array.append(u'Lijado y pintura aplicado')
        if self.poste_m_b_v:
            text_array.append(u'Medidas de base verificada')
        return text_array

    def report_arm(self):
        text_array = []
        pdf = ""
        if self.arm_s_a:
            text_array.append(u'Sujeción adecuada')
        if self.arm_l_p_a:
            text_array.append(u'Lijado y pintura aplicado')
        if self.arm_i_a:
            text_array.append(u'Inclinación adecuada')
        return text_array

    @api.multi
    @api.constrains('stage_id')
    def check_stage(self):
        for record in self:
            if record.stage_id:
                if self.env.ref('helpdesk.group_helpdesk_technical') in self.env.user.groups_id and record.stage_id.sequence > 2:
                    raise ValidationError('No estas autorizado para a cambiar a %s' % record.stage_id.name)

    @api.model
    def create(self, vals):
        vals['name'] = self.env['ir.sequence'].next_by_code('helpdesk.corrective.cabinet')
        if vals.get('point_id'):
            point = self.env['helpdesk.point'].browse(vals.get('point_id'))
            vals['delegate_id'] = point.center_id.delegate_id.id
            vals['latitude'] = point.latitude
            vals['longitude'] = point.longitude
            vals['address'] = point.address
            vals['state_id']= point.state_id.id
            vals['city_id'] = point.city_id.id
            vals['address_reference'] = point.reference
            vals['box_type'] = point.box_type
            vals['poste_type'] = point.poste_type
            vals['arm_type'] = point.arm_type
            vals['serial_fot'] = point.serial_fot
            vals['serial_tran_d'] = point.serial_tran_d
            vals['serial_tran_e'] = point.serial_tran_e
            vals['serial_ups'] = point.serial_ups
            vals['serial_fot_a'] = point.serial_fot
            vals['serial_tran_d_a'] = point.serial_tran_d
            vals['serial_tran_e_a'] = point.serial_tran_e
            vals['serial_ups_a'] = point.serial_ups
            vals['delegate_phone'] = point.center_id.personal_number
            corrective_camera_list = []
            for camera in point.camera_ids:
                corrective_camera_list.append((0,0,{'camera_id':camera.id}))
            vals['corrective_camera_ids'] = corrective_camera_list
        corrective = super(HelpdeskCorrectiveCabinet, self).create(vals)
        # if 'technical_id' in vals:
        #     tmpl = self.env.ref('helpdesk.email_asign_corrective')
        #     mail_id = tmpl.send_mail(corrective.id)
        #     mail = self.env['mail.mail'].browse(mail_id)
        #     mail.send(raise_exception=True)
        return corrective

    @api.multi
    def write(self, vals):
        point_id = vals.get('point_id',self.point_id.id)
        name = vals.get('name',self.name)
        if point_id:
            point = self.env['helpdesk.point'].browse(point_id)
            point_values = {}
            if vals.get('latitude'):
                point_values['latitude'] = vals['latitude']
            if vals.get('longitude'):
                point_values['longitude'] = vals['longitude']
            if vals.get('address'):
                point_values['address'] = vals['address']
            if vals.get('state_id'):
                point_values['state_id'] = vals['state_id']
            if vals.get('city_id'):
                point_values['city_id'] = vals['city_id']
            if vals.get('address_reference'):
                point_values['reference'] = vals['address_reference']
            if vals.get('box_type'):
                point_values['box_type'] = vals['box_type']
            if vals.get('poste_type'):
                point_values['poste_type'] = vals['poste_type']
            if vals.get('arm_type'):
                point_values['arm_type'] = vals['arm_type']

            # Seriales
            if vals.get('serial_fot'):
                point_values['serial_fot'] = vals['serial_fot']
            if vals.get('serial_tran_d'):
                point_values['serial_tran_d'] = vals['serial_tran_d']
            if vals.get('serial_tran_e'):
                point_values['serial_tran_e'] = vals['serial_tran_e']
            if vals.get('serial_ups_a'):
                point_values['serial_ups'] = vals['serial_ups']
            point.write(point_values)
            corrective_camera_list = []

            # cambiar camaras cuando cambian punto
            if vals.get('point_id'):
                camera_ids = []
                for camera in self.corrective_camera_ids:
                    if not camera.camera_id.point_id.id == point_id:
                        corrective_camera_list.append((2, camera.id,))
                    else:
                        camera_ids.append(camera.camera_id.id)
                for camera in point.camera_ids:
                    if camera.id not in camera_ids:
                        corrective_camera_list.append((0, 0, {'camera_id': camera.id}))
                vals['corrective_camera_ids'] = corrective_camera_list

        # Sincronizacion formulario y ticket
        ticket_id = vals.get('ticket_id', self.ticket_id.id)
        if vals.get('stage_id') and ticket_id:
            ticket = self.env['helpdesk.ticket'].browse(ticket_id)
            ticket.sudo().write({'stage_id':vals.get('stage_id')})

        # tratamiento de imagenes
        img_info = ''
        # foto caja antes
        box_photo_before = vals.get('box_photo_before',self.box_photo_before)
        box_photo_before_name = vals.get('box_photo_before_name',self.box_photo_before_name)
        img_info,vals['box_photo_before_name'] = self._img_batch(box_photo_before,box_photo_before_name,img_info
                                                                 ,"_caja_antes.","Caja antes: ",name)
        # foto caja despues
        box_photo_after = vals.get('box_photo_after', self.box_photo_after)
        box_photo_after_name = vals.get('box_photo_after_name', self.box_photo_after_name)
        img_info, vals['box_photo_after_name'] = self._img_batch(box_photo_after, box_photo_after_name, img_info
                                                                  , "_caja_despues.", "Caja despues: ",name)

        vals['img_info'] = img_info

        corrective = super(HelpdeskCorrectiveCabinet, self).write(vals)

        # # envio de correo
        # if vals.get('technical_id',False):
        #     tmpl = self.env.ref('helpdesk.email_asign_corrective')
        #     mail_id = tmpl.send_mail(self.id)
        #     mail = self.env['mail.mail'].browse(mail_id)
        #     mail.send(raise_exception=True)
        return corrective

    @api.model
    def set_coordenate(self, id, latitude, longitude):
        if not id:
            raise ValidationError("Primero debes Guardar el Formulario")
        corrective = self.browse(id)
        corrective.latitude = latitude
        corrective.longitude = longitude

    def button_dummy(self):
        pass

    @api.model
    def _read_group_stage_ids(self, stages, domain, order):
        search_domain = [('sequence', '>=', 0)]
        stage_ids = stages._search(search_domain, order=order, access_rights_uid=SUPERUSER_ID)
        return stages.browse(stage_ids)


    def _img_batch(self,image,img_name,img_info,prefix_name,prefix_info,name):
        new_name=""
        if image:
            img = ImageMetaData.ImageMetaData(cStringIO.StringIO(base64.b64decode(image)))
            img_info += prefix_info + str(img.get_lat_lng()[0]) +" "+ str(img.get_lat_lng()[1])+"\n"
            if img_name:
                if len(img_name.split('.'))>1:
                    new_name = name + prefix_name +img_name.split('.')[-1]
        return img_info, new_name

