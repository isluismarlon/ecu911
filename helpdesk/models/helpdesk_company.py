# -*- coding: utf-8 -*-

from odoo import fields, models, api
from odoo.exceptions import UserError
from odoo.tools.translate import _

class Company(models.Model):
    _name = 'helpdesk.company'

    name = fields.Char('Nombre')
    user_ids = fields.One2many('helpdesk.technical',inverse_name='company_id', string=u'Técnicos')
    email = fields.Char()
    center_ids = fields.Many2many('helpdesk.center',string='Centro')


