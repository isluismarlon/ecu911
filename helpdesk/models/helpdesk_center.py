# -*- coding: utf-8 -*-

from odoo import fields, models, api
from odoo.exceptions import UserError
from odoo.tools.translate import _


class Center(models.Model):
    _name = 'helpdesk.center'
    _inherit = ['mail.thread']

    def _get_delgate_domain(self):
        id = self.env.ref('helpdesk.group_helpdesk_delegate').id
        return [('groups_id', '=', id)]

    name = fields.Char(u'# Zona', required=True, index=True, track_visibility='onchange')
    active = fields.Boolean('Activo', default=True)
    delegate_id = fields.Many2one('res.users', string='Delegado', domain=_get_delgate_domain, track_visibility='onchange')
    point_ids = fields.One2many('helpdesk.point', string='Puntos', inverse_name='center_id', track_visibility='onchange')
    personal_number = fields.Char(u'Número Personal',track_visibility='onchange')
    institutional_number =  fields.Char(u'Número Institucional',track_visibility='onchange')
    tech_number = fields.Char(u'Número Técnologia',track_visibility='onchange')
    email = fields.Char(track_visibility='onchange')
    address = fields.Char(u'Dirección',track_visibility='onchange')

