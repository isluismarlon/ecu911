# -*- coding: utf-8 -*-

from odoo import api, fields, models

AVAILABLE_PRIORITIES = [
    ('0', 'Normal'),
    ('1', 'Low'),
    ('2', 'High'),
    ('3', 'Very High'),
]


class Stage(models.Model):
    _name = "helpdesk.stage"
    _description = "Stage of case"
    _rec_name = 'name'
    _order = "sequence, name, id"

    name = fields.Char('Stage Name', required=True, translate=True)
    sequence = fields.Integer('Sequence', default=1, help="Used to order stages. Lower is better.")
    is_close = fields.Boolean('Closing Kanban stages', help='Tickets in this stages are considered as done.')
    fold = fields.Boolean('Folded',
                          help='This stage is folded in the kanban view '
                               'when there are no records in that stage to display.')