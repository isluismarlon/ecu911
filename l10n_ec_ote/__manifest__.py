# -*- coding: utf-8 -*-

{
    'name': 'Ecuador OTE',
    'version': '1.0',
    'author': 'Manexware',
    'category': 'Generic Modules/Base',
    'summary': 'Geopolitical Information.',
    'license': 'LGPL-3',
    'contributors': [
        'Manuel Vega <manuel.vega@manexware.com>',
    ],
    'website': 'http://manexware.com',
    'depends': [],
    'data': [
        'views/res_state_city_views.xml',
        'views/res_partner_views.xml',
        'views/res_company_views.xml',
        'data/res_country_state.xml',
        'data/res_state_city.xml',
        'data/res_city_parish.xml',
        'data/res_country.xml',
        'security/ir.model.access.csv'
    ],
    'installable': True,
    'auto_install': False,
    'application': False,
}
