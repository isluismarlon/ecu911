# -*- coding: utf-8 -*-
from odoo import models, fields


class CityParish(models.Model):
    _name = 'res.city.parish'

    city_id = fields.Many2one('res.state.city', ondelete='restrict', string="Town", )
    name = fields.Char(string="Parish", )
    code = fields.Char(string="Code", )
