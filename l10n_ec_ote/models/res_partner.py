# -*- coding: utf-8 -*-
from stdnum.ec import ruc, ci

from odoo import models, fields, api
from odoo.exceptions import ValidationError


class ResPartner(models.Model):
    _inherit = 'res.partner'

    city_id = fields.Many2one('res.state.city', 'City', ondelete='restrict', domain="[('state_id','=',state_id)]")
    parish_id = fields.Many2one(
        'res.city.parish', ondelete='restrict', string="Parish",domain="[('city_id', '=', city_id)]")


    @api.multi
    @api.onchange('city_id')
    def _onchange_city_id(self):
        for record in self:
            if record.city_id:
                record.city = self.city_id.name.capitalize()
            else:
                record.city = None

    @api.multi
    def onchange_state(self, state_id):
        if state_id:
            return {'value': {'city_id': None, 'city': None}}
        return super(ResPartner, self).onchange_state(state_id)

    @api.onchange('country_id')
    def _onchange_country_id(self):
        if self.country_id:
            self.state_id = None
            self.city_id = None
            self.parish_id = None

